package calculator.exceptions;

public class NoCalculations extends RuntimeException{

	public NoCalculations() {
		super("Nothing to calculate!");
	}
}
