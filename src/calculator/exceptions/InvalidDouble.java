package calculator.exceptions;

public class InvalidDouble extends RuntimeException{

	public InvalidDouble() {
		super("Invalid decimal!");
	}
}
