package calculator.exceptions;

public class MissingBracket extends RuntimeException{

	public MissingBracket() {
		super("A bracket is missing!");
	}
}
