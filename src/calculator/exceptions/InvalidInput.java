package calculator.exceptions;

public class InvalidInput extends RuntimeException{
	
	public InvalidInput() {
		super("Invalid input!");
	}

}
