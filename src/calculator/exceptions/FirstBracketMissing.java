package calculator.exceptions;

public class FirstBracketMissing extends RuntimeException{

	public FirstBracketMissing() {
		super("First bracket is missing!");
	}
}
