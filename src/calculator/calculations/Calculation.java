package calculator.calculations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Pattern;

import calculator.dtos.TwoNumbers;
import calculator.exceptions.FirstBracketMissing;
import calculator.exceptions.InvalidDouble;
import calculator.exceptions.InvalidInput;
import calculator.exceptions.MissingBracket;
import calculator.exceptions.NoCalculations;



public class Calculation {
	
	private MathOps math = new MathOps();

	public String calculate(String s) {

		Double result = null;

		// removing empty spaces and double operations
		s = s.trim().replaceAll(" ","").replaceAll("\\-\\-", "+").replaceAll("\\-\\+", "-").replaceAll("\\+\\-", "-");

		// removes the trailing operator problem!!! 
		char operator = s.charAt(s.length()-1);

		if(operator == '/' || operator == '*' || operator == '^' || operator == '+' || operator == '-') s = s.substring(0, s.length()-1);
		// searches for any letters in the input /[^0-9\-+*.\/^()]/gm
		if(Pattern.compile("[^0-9\\-+*.\\/^()]").matcher(s).find()) throw new InvalidInput();
		// searches for anything to calculate, if there is no calculation sign, throws an exception
		if(!Pattern.compile("[\\-+*\\/^]").matcher(s).find()) throw new NoCalculations();
		// counting both brackets in the input
		int bracketsLeft = 0;
		int bracketsRight = 0;
		for(int i = 0; i<s.length(); i++) {
			if(s.charAt(i)=='(') bracketsLeft++;
			if(s.charAt(i)==')') bracketsRight++;
		}

		// if number of both side brackets/parentheses/whatever isn't the same, something done goofed up
		if(bracketsLeft!=bracketsRight) throw new MissingBracket();

		// if first ')' index is less than first '(' index, throw an error

		if(s.indexOf(')')<s.indexOf('(')) throw new FirstBracketMissing();

		String calculated = calculations(s);
		result = Double.valueOf(calculated);
		// check whether the result is a double or not and convert it to string
		return result%1 != 0? calculated: String.valueOf(result.intValue());
	}

	public TwoNumbers getNumbers(String s, int marker) {

		//print("Get numbers string> "+s+"| Marker >"+marker);

		Integer leftIndex = null;
		Integer rightIndex = null;

		// getting left side of the operation
		StringBuilder left = new StringBuilder();
		for(int i = marker-1; i>=0; i--) { // going backwards for loop because it's looping from the operator to the left
			char c = s.charAt(i);
			if(c!='+' && c!='-' && c!='/' && c!='*' && c!='^') {// as long it's a number, keep adding it to the StringBuilder
				left.append(c);
				leftIndex = i; // updating left index for re-insertion of the result
			} else {
				if(c=='-') { // if it's a '-' check if the next character(if available)  
					if(i==0 || (s.charAt(i-1)=='+' || s.charAt(i-1)=='-' || s.charAt(i-1)=='*' || s.charAt(i-1)=='/' || !Character.isDigit(s.charAt(i-1)))) {
						left.append(c);
						leftIndex = i;
					}
				}
				break;
			}
		}

		left.reverse();// needs to be reversed because the last number is actually the first
		
		// taking care of the operator
		if(left.charAt(left.length()-1)=='-' || left.charAt(left.length()-1)=='+') {// if there is one
			String numbs = left.substring(0, left.length()-1);
			String sign = left.substring(left.length()-1);
			left = new StringBuilder(sign+numbs);// it must come at the beginning of the string
		}

		if(left.charAt(0)=='.') throw new InvalidDouble();// rare off chance that the decimal starts without a number 
		
		// same as before but the direction is from left to right so there is no need for reversing the string
		StringBuilder right = new StringBuilder();
		for(int i = marker+1; i<s.length(); i++) {
			char c = s.charAt(i);
			if(c!='+' && c!='-' && c!='/' && c!='*' && c!='^' && c!='(' && c!=')') {
				right.append(c);
				rightIndex = i;
			} else {
				if(c=='-' && i == marker+1) {
					right.append(c);
					rightIndex = i;
				}else {
					break;
				}
			}
		}

		if(right.charAt(0)=='.') throw new InvalidDouble();// same as before
		
		// storing double values, BigDecimal seems to solve the problem of having 1.0e-1 plaguing the rest of the code
		// not really sure why, how or does it actually work, will look it up later
		double leftNumber = new BigDecimal(left.toString()).setScale(6, RoundingMode.HALF_UP).doubleValue();
		double rightNumber = new BigDecimal(right.toString()).setScale(6, RoundingMode.HALF_UP).doubleValue();
		
		return new TwoNumbers(leftNumber, rightNumber, leftIndex, rightIndex);// pack it in a DTO and send it on it's way

	}

	public String calculations(String string) {
		
		// this should solve the problem of double values being recognized as subtraction (example 0.3e-1) 
		// and messing up everything 
		// edit -> so far so good
		// yeah, it's working but not because of this
		if(Pattern.compile("[A-Za-z]").matcher(string).find()) return string;

		string = string.replaceAll("\\+\\-", "-").replaceAll("\\-\\+", "-").replaceAll("\\-\\-", "+");
		TwoNumbers tn;
		String result;
		Integer power;
		Integer division;
		Integer multiplication;
		Integer subtraction;
		Integer addition;
		Integer rightBracket;

		// '^' needs to be addressed first because it must take precedence over the brackets because of the possible 
		// negative numbers that need to be calculated to the power of n
		while((power = string.indexOf('^'))!=-1) {
			// check whether the next left char is a ')'
			if(string.charAt(power-1)==')') {
				// if it is, find the corresponding '('
				int rightMarker = power-1;
				int leftMarker = -1;
				int r = 0; // count ')'
				int l = 0;// count '('
				for(int i = rightMarker; i>=0; i--) {
					char c = string.charAt(i);
					if(c==')') r++; // increase the number of found ')' 
					if(c=='(') { // if char is a '(' 
						l++; // increase the number of found '('
						if(r == l) { // if the number of '(' is equal to the number of ')'
							leftMarker = i; // set the marker for the corresponding '('
							break; // break the loop
						}
					}
				} // for loop end
				
				// cut out the bracket content
				String left = string.substring(0, leftMarker);
				String newString = string.substring(leftMarker+1, rightMarker);
				String right = string.substring(rightMarker+1, string.length());
				// calculate what's inside the brackets
				String newResult = calculations(newString); // this is a recursive call, not sure if it's the best idea but it works
				
				string = left+newResult+right; // put it back together
				power = string.indexOf('^'); // set new index of first found '^' since the original String has changed
			}
			//System.out.println("POOOOWAAAAAAAHHHH...unlimited POOOOWAAAAAAAHHHH..."+string);
			tn = getNumbers(string, power); // find the numbers for pow function together with the indexes for splicing the string
			
			// calculate the pow then get it to string with max 6 decimals rounded up
			result = new BigDecimal(math.pow(tn.getLeft(), tn.getRight())).setScale(6, RoundingMode.HALF_UP).toString();
			// putting it back together
			string = string.substring(0, tn.getLeftIndex())+result+string.substring(tn.getRightIndex()+1, string.length());
			// this shouldn't happen at this point but...
			string = string.replaceAll("\\+\\-", "-").replaceAll("\\-\\+", "-").replaceAll("\\-\\-", "+");
		}
		
		// get the index of the first ')' bracket
		while((rightBracket = string.indexOf(')'))!=-1) { // while there are brackets -> calculate
			
			int leftBracket = -1;
			// at this point, there has to be a left side bracket, if there weren't one, exception would've fired off earlier (line 44)
			for(int i = rightBracket-1; i>=0; i--) {
				char c = string.charAt(i);
				if(c=='(') {
					leftBracket = i;
					break;
				}
			}
			if(leftBracket == -1) throw new MissingBracket();// just in case if exception hadn't fired earlier (line 180)
			
			// separate the string
			String leftString = string.substring(0, leftBracket);
			String calculateString = string.substring(leftBracket+1, rightBracket); // this one will be calculated further
			String rightString = string.substring(rightBracket+1, string.length());
			// recursive call - it calculates the most inner brackets 
			// there shoudln't be other brackets so it should only calculate other operations
			String bracketResult = calculate(calculateString);// I wanted to avoid this but I couldn't figure how to do it without recursion 
			
			string = leftString+bracketResult+rightString;
		}
		// from here on now, it's pretty much the same as before just with a different operator
		while((division = string.indexOf('/'))!=-1) {
			tn = getNumbers(string, division);
			String newResult = new BigDecimal(math.divide(tn.getLeft(), tn.getRight())).setScale(6, RoundingMode.HALF_UP).toString();
			String leftString = string.substring(0, tn.getLeftIndex());
			String rightString = string.substring(tn.getRightIndex()+1, string.length());
			string = leftString+newResult+rightString;
			string = string.replaceAll("\\+\\-", "-").replaceAll("\\-\\+", "-").replaceAll("\\-\\-", "+");
		}
		// this one is shorter because I got bored of typing the same thing over and over again
		while((multiplication = string.indexOf('*'))!=-1) {
			tn = getNumbers(string, multiplication);
			result = new BigDecimal(math.multi(tn.getLeft(), tn.getRight())).setScale(6, RoundingMode.HALF_UP).toString();
			string = string.substring(0, tn.getLeftIndex())+result+string.substring(tn.getRightIndex()+1, string.length());
			string = string.replaceAll("\\+\\-", "-").replaceAll("\\-\\+", "-").replaceAll("\\-\\-", "+");
		}
		// ...
		while((subtraction = string.lastIndexOf('-'))!=-1 && subtraction!=0) {
			tn = getNumbers(string, subtraction);
			result = new BigDecimal(math.sub(tn.getLeft(), tn.getRight())).setScale(6, RoundingMode.HALF_UP).toString();
			string = string.substring(0, tn.getLeftIndex())+result+string.substring(tn.getRightIndex()+1, string.length());
			// while calculating subtraction, if the numbers give negative value, two -- shouldn't become + because that would 
			// change the next subtraction to addition 
			// example 1-2-3 => -2-3=-5 => 1-5 if it were to replace 1--5 with a +, the calculation would be wrong since it would be 1+5
			// edit -> it's working fine, couldn't get it to fail so far
			string = string.replaceAll("\\+\\-", "-").replaceAll("\\-\\+", "-").replaceAll("\\-\\-", "-");
		}

		// ...there must have been a shorter way of doing this.
		// Maybe something with a switch with maybe storing functions in a hashMap or something
		// but for better or for worse, it's done now. Finally.
		while((addition = string.indexOf('+'))!=-1) {
			tn = getNumbers(string, addition);
			result = new BigDecimal(math.add(tn.getLeft(), tn.getRight())).setScale(6, RoundingMode.HALF_UP).toString();
			string = string.substring(0, tn.getLeftIndex())+result+string.substring(tn.getRightIndex()+1, string.length());
			string = string.replaceAll("\\+\\-", "-").replaceAll("\\-\\+", "-").replaceAll("\\-\\-", "+");
		}

		// forgot to implement sqrt... maybe later.
		
		
		// BigDecimal scale is set to 6 decimals, trailing zeros will occur and they must be striped down 
		return new BigDecimal(string).stripTrailingZeros().toString();
	}

}
