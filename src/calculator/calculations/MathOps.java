package calculator.calculations;

public class MathOps {
	
	public double add(double a, double b){
		return a+b;
	}
	
	public double sub(double a, double b){
		return a-b;
	}
	
	public double divide(double a, double b){
		return a/b;
	}
	
	public double multi(double a, double b){
		return a*b;
	}
	
	public double pow(double a, double b){
		double result = 1;
		if(b>0) {
			for(int i = 0; i<b; i++) result*=a;
		}
		if(b<0) {
			for(int i = (int) b; i<0; i++) result/=a;
		}
		return result;
	}

}
