package calculator.dtos;

public class TwoNumbers {
	
	private double left;
	private int leftIndex;
	private double right;
	private int rightIndex;
	
	public TwoNumbers(double left, double right, int leftIndex, int rightIndex) {
		this.left = left;
		this.right = right;
		this.leftIndex = leftIndex;
		this.rightIndex = rightIndex;
	}
	
	public TwoNumbers() {}
	
	public void setLeft(double left) {
		this.left = left;
	}
	
	public double getLeft() {
		return this.left;
	}
	
	public void setLeftIndex(int leftIndex) {
		this.leftIndex = leftIndex;
	}
	
	public int getLeftIndex() {
		return this.leftIndex;
	}
	
	public void setRight(double right) {
		this.right = right;
	}
	
	public double getRight() {
		return this.right;
	}
	
	public void setRightIndex(int rightIndex) {
		this.rightIndex = rightIndex;
	}
	 
	public int getRightIndex() {
		return this.rightIndex;
	}
}
