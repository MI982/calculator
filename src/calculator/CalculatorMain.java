package calculator;

public class CalculatorMain {
	
	private static CalculatorFrame frame;
	
	public static void main(String...strings) {
		
		createFrame();
		frame.initialize();
		
	}

	private static void createFrame() {
		frame = new CalculatorFrame("Calculator");
	}
}
