package calculator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox.KeySelectionManager;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import calculator.calculations.Calculation;

public class CalculatorFrame extends JFrame implements ActionListener, KeyListener{
	
	private static final String CALCULATE = "=";
	private static final int DEFAULTWIDTH = 400;
	private static final int DEFAULTHEIGHT = 300;
	private static final String DEFAULTINPUTLABELTEXT = "Example; 1+(2-3)*4/5^6";

	private JButton button;
	private JLabel resultLabel, inputLabel;
	private Calculation calculation;

	
	public CalculatorFrame(String string) {
		super(string);
	}

	public void initialize() {
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationByPlatform(true);
		this.setPreferredSize(new Dimension(DEFAULTWIDTH, DEFAULTHEIGHT));
		this.setResizable(false);
		this.addKeyListener(this);
		
		Container mainFrame = this.getContentPane();
		mainFrame.setLayout(new BorderLayout(5, 5));
		mainFrame.setBackground(Color.BLACK);
		
		mainFrame.add(createNorthPanel(), BorderLayout.NORTH);
		//mainFrame.add(createWestPanel(), BorderLayout.WEST);
		mainFrame.add(createCenterPanel(), BorderLayout.CENTER);
		this.pack();
		this.setVisible(true);
		
		calculation = new Calculation();
	}
	
	private Component createNorthPanel() {
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new FlowLayout(FlowLayout.TRAILING, 5, 5));
		northPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 5));
		northPanel.setBackground(Color.LIGHT_GRAY);
		
		inputLabel = new JLabel();
		inputLabel.setText(DEFAULTINPUTLABELTEXT);
		inputLabel.setFont(new Font("Arial", Font.BOLD, 18));
		inputLabel.setForeground(Color.DARK_GRAY);
		
		northPanel.add(inputLabel);
		
		button = new JButton(CALCULATE);
		prepareButton(button);
		
		northPanel.add(button);
		
		resultLabel = new JLabel();
		resultLabel.setText("?");
		resultLabel.setFont(new Font("Arial", Font.BOLD, 18));
		
		northPanel.add(resultLabel);
		
		return northPanel;
	}
	
	
	private Container createCenterPanel() {
		JPanel centerPanel = new JPanel();
		
		centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		centerPanel.setBackground(Color.LIGHT_GRAY);
		centerPanel.setBorder(BorderFactory.createLineBorder(Color.black, 5, false));
		
		centerPanel.add(createNumberPanel());
		centerPanel.add(createOperationsPanel());
		
		return centerPanel;
	}
	
	private Container createNumberPanel() {
		JPanel numberPanel = new JPanel();
		numberPanel.setBackground(Color.DARK_GRAY);
		numberPanel.setLayout(new GridLayout(4, 3, 5, 5));
		numberPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		for(int i = 9; i>0; i--) {
			JButton button = new JButton(String.valueOf(i));
			prepareButton(button);
			numberPanel.add(button);
		}
		
		JButton dotButton = new JButton(".");
		prepareButton(dotButton);
		
		JButton zeroButton = new JButton("0");
		prepareButton(zeroButton);
		
		JButton clearButton = new JButton("CE");
		prepareButton(clearButton);
		
		
		numberPanel.add(dotButton);
		numberPanel.add(zeroButton);
		numberPanel.add(clearButton);
		
		return numberPanel;
	}
	
	private Container createOperationsPanel() {
		JPanel operationsPanel = new JPanel();
		operationsPanel.setBackground(Color.DARK_GRAY);
		operationsPanel.setLayout(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(3, 3, 3, 3);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 0;
		JButton plus = new JButton("+");
		prepareButton(plus);
		operationsPanel.add(plus, constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		JButton times = new JButton("*");
		prepareButton(times);
		operationsPanel.add(times, constraints);
		
		constraints.gridx = 2;
		constraints.gridy = 0;
		JButton left = new JButton("(");
		prepareButton(left);
		operationsPanel.add(left, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 1;
		JButton minus = new JButton("-");
		prepareButton(minus);
		operationsPanel.add(minus, constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 1;
		JButton divide = new JButton("/");
		prepareButton(divide);
		operationsPanel.add(divide, constraints);
		
		constraints.gridx = 2;
		constraints.gridy = 1;
		JButton right = new JButton(")");
		prepareButton(right);
		operationsPanel.add(right, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 2;
		JButton power = new JButton("^");
		prepareButton(power);
		operationsPanel.add(power, constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 2;
		JButton del = new JButton("del");
		prepareButton(del);
		operationsPanel.add(del, constraints);
		
		
		return operationsPanel;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		String action = e.getActionCommand();
		if(action.equalsIgnoreCase(CALCULATE)) {
			String input = inputLabel.getText();
			if(!input.isBlank() && !input.equals(DEFAULTINPUTLABELTEXT)) {
				String result;
				try{ 
					result = calculation.calculate(input);
				} catch(Exception exception) {
					result = exception.getMessage();
					System.out.println(result);
					resultLabel.setForeground(Color.red);
				}
				resultLabel.setText(result);
			}
			//checkSize();
		} else if(action.equalsIgnoreCase("CE")) {
			inputLabel.setText(DEFAULTINPUTLABELTEXT);
			if(resultLabel.getForeground() == Color.RED) resultLabel.setForeground(Color.BLACK);
			resultLabel.setText("?");
			//checkSize();
		} else if(action.equalsIgnoreCase("del")) {
			inputLabel.setText(inputLabel.getText().substring(0, inputLabel.getText().length()-1));
			if(!resultLabel.getText().equalsIgnoreCase("?")) resultLabel.setText("?");
			if(resultLabel.getForeground() == Color.RED) resultLabel.setForeground(Color.BLACK);
		} else {
			String inputLabelText = inputLabel.getText().equalsIgnoreCase(DEFAULTINPUTLABELTEXT)? "": inputLabel.getText();
			if(!resultLabel.getText().equalsIgnoreCase("?")) resultLabel.setText("?");
			if(resultLabel.getForeground() == Color.RED) resultLabel.setForeground(Color.BLACK);
			inputLabel.setText(inputLabelText+action);
			//checkSize();
		}
		
	}
	
	private void prepareButton(JButton button) {
		button.setBackground(Color.LIGHT_GRAY);
		button.setForeground(Color.BLACK);
		button.setFont(new Font("Arial", Font.BOLD, 18));
		button.setBorderPainted(false);
		button.setFocusable(false);
		button.addActionListener(this);
	}
	
	private void checkSize() {
		Integer mainFrameWidth = this.getContentPane().getWidth();
		Integer northFrameWidth = inputLabel.getWidth()+button.getWidth()+resultLabel.getWidth();
		if(northFrameWidth > mainFrameWidth) {
			this.setResizable(true);
			this.setPreferredSize(new Dimension(500, northFrameWidth));
			this.setResizable(false);
			this.repaint();
		}
		if(mainFrameWidth > DEFAULTWIDTH && northFrameWidth < DEFAULTWIDTH) {
			this.setResizable(true);
			this.setPreferredSize(new Dimension(500, DEFAULTWIDTH));
			this.setResizable(false);
			this.repaint();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		char key = e.getKeyChar();
		String action = String.valueOf(e.getKeyChar());
		int actionKey = key;
		System.out.println(key);
		if(Pattern.compile("[0-9\\-+*.\\/^()]").matcher(action).find()) {
			String inputLabelText = inputLabel.getText().equalsIgnoreCase(DEFAULTINPUTLABELTEXT)? "": inputLabel.getText();
			if(!resultLabel.getText().equalsIgnoreCase("?")) resultLabel.setText("?");
			if(resultLabel.getForeground() == Color.RED) resultLabel.setForeground(Color.BLACK);
			inputLabel.setText(inputLabelText+action);
		} else if(action.equalsIgnoreCase(CALCULATE)) {
			String input = inputLabel.getText();
			if(!input.isBlank() && !input.equals(DEFAULTINPUTLABELTEXT)) {
				String result;
				try{ 
					result = calculation.calculate(input);
				} catch(Exception exception) {
					result = exception.getMessage();
					System.out.println(result);
					resultLabel.setForeground(Color.red);
				}
				resultLabel.setText(result);
			}
		} else if(actionKey == KeyEvent.VK_DELETE) {
			inputLabel.setText(DEFAULTINPUTLABELTEXT);
			if(resultLabel.getForeground() == Color.RED) resultLabel.setForeground(Color.BLACK);
			resultLabel.setText("?");
		} else if(actionKey == KeyEvent.VK_BACK_SPACE) {
			inputLabel.setText(inputLabel.getText().substring(0, inputLabel.getText().length()-1));
			if(!resultLabel.getText().equalsIgnoreCase("?")) resultLabel.setText("?");
			if(resultLabel.getForeground() == Color.RED) resultLabel.setForeground(Color.BLACK);
		} else if(actionKey == KeyEvent.VK_ESCAPE) {
			this.dispose();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
